import pickle
from RSA import RSA


def read_file(path):
    with open(path, "r", encoding="utf-8") as file:
        return file.read()


def read_file_pickle(path):
    with open(path, "rb") as file:
        return pickle.load(file)


def write_to_file_pickle(path, message):
    with open(path, "wb") as file:
        pickle.dump(message, file)


def demo_rsa(rsa: RSA):
    rsa_encode(rsa, path_from="text.txt", path_to="message.txt")
    result = rsa_decode(rsa, path_from="message.txt")
    message = rsa.codes_to_char(result)
    print(f"Сообщение: {message}")


def rsa_encode(rsa: RSA, path_from, path_to):
    encode_codes, e = rsa.get_encode_string(read_file(path_from))
    write_to_file_pickle(path_to, encode_codes)


def rsa_decode(rsa: RSA, path_from):
    e = rsa.get_e(rsa.euler)
    message = read_file_pickle(path_from)
    result = rsa.get_decode_string(message, e)
    return result


if __name__ == '__main__':
    rsa = RSA(211, 223)
    # Объединённые процессы (кодирование и декодирование)
    # demo_rsa(rsa)

    # Кодирование сообщения
    # print("Кодирование сообщения".center(80, "~"))
    # rsa_encode(rsa, "text.txt", "message.txt")

    # Декодирование сообщения
    print("Декодирование сообщения".center(80, "~"))
    result = rsa_decode(rsa, "message.txt")
    message = rsa.codes_to_char(result)
    print(f"Сообщение: {message}")
