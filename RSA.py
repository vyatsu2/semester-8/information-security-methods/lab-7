import math


class RSA:
    def __init__(self, p, q):
        self.p = p
        self.q = q
        self.n = p * q
        self.euler = (p - 1) * (q - 1)

    def get_encode_string(self, string):
        codes = self.get_array_simbols_code(string)
        print(f"Коды символов: {codes}")

        e = self.get_e(self.euler)
        print(f"Взаимнопростое с Эйлером: {e}")

        encode_codes = self.get_encode_array(codes, e)
        print(f"Закодированное сообщение: {encode_codes}")
        return encode_codes, e

    def get_decode_string(self, encode_codes, e):
        d = self.get_d(e)
        print(f"Найденное d: {d}")

        decode_codes = self.get_decode_array(encode_codes, d)
        print(f"Раскодированное сообщение: {decode_codes}")
        return decode_codes

    @staticmethod
    def codes_to_char(codes):
        result = ''
        for code in codes:
            result += chr(code)
        return result

    def get_decode_array(self, array, d):
        result = []
        for num in array:
            result.append((num ** d) % self.n)
        return result

    @staticmethod
    def get_e(num):
        e = 2
        while math.gcd(e, num) != 1:
            e += 1
        return e

    def get_d(self, e):
        d = 1
        while (e * d) % self.euler != 1:
            d += 1
        return d

    def get_encode_array(self, array, e):
        result = []
        for num in array:
            result.append((num ** e) % self.n)
        return result

    @staticmethod
    def get_array_simbols_code(string):
        result = []
        for char in string:
            result.append(ord(char))
        return result
